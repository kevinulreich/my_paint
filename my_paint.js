$(document).ready(function()
{
	var canvas = $('canvas');
	var ctx = canvas[0].getContext("2d");
	var myColor = 'black';
	var size = 0;
	var alpha;
	var pos1_x1;
	var	pos1_y1;
	var	pos_x2;
	var	pos_y2;
	var tool = null;
	var action = true;
	var fill = false;
	var radius = 0;
	var bIsDrawing = false;
	var nStartX = 0;
	var nStartY = 0;
	var xx;
	var yy;
	var action = false;

	$('.btn').each(function()
	{
		$(this).click(function()
		{
			tool = $(this).attr("id");
		});
	});

	canvas.mousedown(function(event){
		switch(tool)
		{
			case 'eraser':
			down();
			case 'pen':
			down();
			case 'rectangle':
			rectangleDown();
			break;
			case 'line':
			down();
			break;
			case 'circle':
			circleDown();
			break;
		}
	});

	canvas.mousemove(function(event)
	{
		switch(tool)
		{
			case 'eraser':
			move();
			break;
			case 'pen':
			move();
			break;
			case 'circle':
			circleMove();
			break;
		}
	});

	canvas.mouseup(function(event)
	{
		switch(tool)
		{
			case 'eraser':
			up();
			break;
			case 'pen':
			up();
			break;
			case 'rectangle':
			rectangleUp();
			break;
			case 'line':
			up();
			break;
			case 'circle':
			circleUp();
			break;
		}
	});

	$('#check').click(function (){
		fill = $('#check').is(':checked');
	});

	$('.size').change(function() 
	{
		size = $(this).val();
	});

	$('.alpha').change(function() 
	{
		alpha = $(this).val();
	});

	$('.color').on('input', function()
	{
		myColor = $(this).val();
	});


	$('#clear').click(function()
	{
		ctx.clearRect(0, 0, 700, 500);
	});


	function style()
	{
		ctx.globalAlpha = alpha;
		ctx.lineWidth = size;
		if (tool == 'eraser')
		{
			ctx.strokeStyle = 'white';
		}
		else
		{
			ctx.strokeStyle = myColor;
		}
		ctx.lineJoin = "round";
		ctx.lineCap = "round";
	}

	function down()
	{
		
		action = true
		style();
		var x = event.offsetX;
		var y = event.offsetY;
		var xx = event.offsetX;
		var yy = event.offsetY;
		ctx.beginPath(x, y);
		if(tool == 'line')
		{
			ctx.moveTo(x, y);
		}
	}

	function move()
	{
		if (action != true)
		{
			return;
		}
		var x = event.offsetX;
		var y = event.offsetY;
		ctx.moveTo(xx,yy);
		ctx.lineTo(x,y);
		ctx.stroke();
		style();
	}

	function up()
	{
		if (tool == 'pen' || tool == 'eraser' )
		{
			action = false
		}
		style();
		var x = event.offsetX;
		var y = event.offsetY;
		ctx.lineTo(x,y);
		ctx.stroke();
		action = false;
	}

	function rectangleDown()
	{
		pos1_x1 = event.offsetX;
		pos1_y1 = event.offsetY;
		ctx.beginPath();
	}
	
	function rectangleUp()
	{
		style();
		pos_x2 = event.offsetX - pos1_x1;
		pos_y2 = event.offsetY - pos1_y1;
		if (fill == true)
		{
			ctx.rect(pos1_x1, pos1_y1, pos_x2, pos_y2);
			ctx.fillStyle = myColor;
			ctx.fill();
		}
		else
		{
			ctx.strokeRect(pos1_x1, pos1_y1, pos_x2, pos_y2);
		}
		ctx.closePath();
	}

	function circleDown()
	{
		nStartX = event.clientX;
		nStartY = event.clientY;
		bIsDrawing = true;
		radius = 0;
	}

	function circleMove()   
	{    
		if(!bIsDrawing)
		{
			return;
		}
		var nDeltaX = nStartX - event.clientX;
		var nDeltaY = nStartY - event.clientY;
		radius = Math.sqrt(nDeltaX * nDeltaX + nDeltaY * nDeltaY)
		ctx.rect(0, 0, canvas.width, canvas.height);
		ctx.beginPath();
		ctx.arc(nStartX, nStartY, radius, 0, Math.PI*2);

	}

	function circleUp()
	{
		style();
		if (fill == true)
		{
			ctx.fillStyle = myColor;
			ctx.fill();	
		}
		else
		{
			ctx.lineWidth = size;
			ctx.stroke();
		}
		bIsDrawing = false;
	}

	var imageLoader = document.getElementById('imageLoader');
	imageLoader.addEventListener('change', handleImage, false);


	function handleImage(e)
	{
		var reader = new FileReader();
		reader.onload = function(event){
			var img = new Image();
			img.onload = function(){
				canvas.width = img.width;
				canvas.height = img.height;
				ctx.drawImage(img,0,0);
			}
			img.src = event.target.result;
		}
		reader.readAsDataURL(e.target.files[0]);     
	}

	$("#save").click(function() {
		var canvas_tmp = document.getElementById("canvas");	
		var lnk = document.createElement('a'), e;
		window.location = canvas_tmp.toDataURL("image/png");
	});
});



